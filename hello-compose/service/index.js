const http = require('http');

http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(`<h1>Hello, ${process.env.MESSAGE}</h1>`);
    console.log(`Received ${req.method} request!`);
}).listen(3030);

console.log(`Listening for ${process.env.MESSAGE} on port 3030...`);
