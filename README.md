# Docker & Docker Compose Hellow World Package

## What does this repo contains?
* `hello-docker` - a simple application to demonstrate `Dockerfile` syntax and how to work with Docker CLI
* `hello-nginx` - an example serving a static HTML page using NGINX to use with the Docker CLI to demonstrate how start long-running process and expose ports
* `hello-compose` - an example showing how to compose multiple services into one multi-container application